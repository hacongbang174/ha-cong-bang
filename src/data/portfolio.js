import emoji from "react-easy-emoji";
import splashAnimation from "../assets/lottie/splashAnimation";
import Images from "../constants/images";

const splashScreen = {
  enabled: true,
  animation: splashAnimation,
  duration: 2000,
};

const illustration = {
  animated: true,
};

const greeting = {
  username: "Hà Công Bằng",
  title: "Xin chào! Tôi là Bằng",
  subTitle: emoji(
    "A passionate Full Stack Software Developer 🚀 having an experience of building Web with JavaScript / Reactjs / Nodejs and some other cool libraries and frameworks."
  ),
  resumeLink:
    "https://drive.google.com/drive/u/1/folders/1JZ4voR2TytSlmVBtm6zhBcpZwikz8Vn7", // Set to empty to hide the button
  displayGreeting: true,
};

const socialMediaLinks = {
  github: "https://github.com/hacongbang174",
  gmail: "hacongbang174@gmail.com",
  gitlab: "https://gitlab.com/hacongbang174",
  facebook: "https://www.facebook.com/profile.php?id=100079073785914",
  stackoverflow:
    "https://stackoverflow.com/users/22549332/c%c3%b4ng-b%e1%ba%b1ng-h%c3%a0",
  display: true,
};

const skillsSection = {
  title: "What I do",
  subTitle: "CRAZY FULL STACK DEVELOPER WHO WANTS TO EXPLORE EVERY TECH STACK",
  skills: [
    emoji(
      "⚡ Develop highly interactive Front end / User Interfaces for your web and mobile applications"
    ),
    emoji("⚡ Progressive Web Applications ( PWA ) in normal and SPA Stacks"),
    emoji(
      "⚡ Integration of third party services such as Firebase/ AWS / Digital Ocean"
    ),
  ],

  softwareSkills: [
    {
      skillName: "html-5",
      fontAwesomeClassname: "fab fa-html5",
    },
    {
      skillName: "css3",
      fontAwesomeClassname: "fab fa-css3-alt",
    },
    {
      skillName: "sass",
      fontAwesomeClassname: "fab fa-sass",
    },
    {
      skillName: "JavaScript",
      fontAwesomeClassname: "fab fa-js",
    },
    {
      skillName: "TypeScript",
      fontAwesomeClassname: "fab fa-js",
    },
    {
      skillName: "reactjs",
      fontAwesomeClassname: "fab fa-react",
    },
    {
      skillName: "nodejs",
      fontAwesomeClassname: "fab fa-node",
    },
    {
      skillName: "npm",
      fontAwesomeClassname: "fab fa-npm",
    },
    {
      skillName: "MySQL-database",
      fontAwesomeClassname: "fas fa-database",
    },
    {
      skillName: "MongoDB",
      fontAwesomeClassname: "fab fa-database",
    },
    {
      skillName: "aws",
      fontAwesomeClassname: "fab fa-aws",
    },
    {
      skillName: "firebase",
      fontAwesomeClassname: "fas fa-fire",
    },
    {
      skillName: "docker",
      fontAwesomeClassname: "fab fa-docker",
    },
  ],
  display: true,
};

const educationInfo = {
  display: true,
  schools: [
    {
      schoolName: "Ho Chi Minh City University of Technology",
      logo: Images.BKHCM_LOGO,
      subHeader: "Port Construction and Marine Structures.",
      duration: "September 2012 - April 2017",
      // desc: "Participated in the research of XXX and published 3 papers.",
      descBullets: [
        // "Lorem ipsum dolor sit amet, consectetur adipiscing elit",
        // "Lorem ipsum dolor sit amet, consectetur adipiscing elit"
      ],
    },
    {
      schoolName: "CODEGYM TRAINING CENTER IN HUẾ",
      logo: Images.CODE_GYM,
      subHeader: "Develop Java fullstack",
      duration: "February 2023 - August 2023",
      desc: "Took courses about Software Engineering, Web Security, Operating Systems, ...",
      descBullets: [
        "Learning programming knowledge from basic to advanced",
        "Back-end web development with JSP & Servlet and Spring MVC",
        "Front-end web development with HTML, CSS, JavaScript, React ",
        "Software development following the Scrum - Agile methodology",
      ],
    },
  ],
};

const techStack = {
  viewSkillBars: true,
  experience: [
    {
      Stack: "Frontend/Design",
      progressPercentage: "70%",
    },
    {
      Stack: "Backend",
      progressPercentage: "90%",
    },
    {
      Stack: "Programming",
      progressPercentage: "70%",
    },
  ],
  displayCodersrank: false,
};

const workExperiences = {
  display: true,
  experience: [
    // {
    //   role: "Software Engineer",
    //   company: "Facebook",
    //   companylogo: require("./assets/images/facebookLogo.png"),
    //   date: "June 2018 – Present",
    //   desc: "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.",
    //   descBullets: [
    //     "Lorem ipsum dolor sit amet, consectetur adipiscing elit",
    //     "Lorem ipsum dolor sit amet, consectetur adipiscing elit"
    //   ]
    // },
    // {
    //   role: "Front-End Developer",
    //   company: "Quora",
    //   companylogo: require("./assets/images/quoraLogo.png"),
    //   date: "May 2017 – May 2018",
    //   desc: "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua."
    // },
    // {
    //   role: "Software Engineer Intern",
    //   company: "Airbnb",
    //   companylogo: require("./assets/images/airbnbLogo.png"),
    //   date: "Jan 2015 – Sep 2015",
    //   desc: "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua."
    // }
  ],
};

const openSource = {
  showGithubProfile: "true",
  display: true,
};

const bigProjects = {
  title: "Projects",
  subtitle: "",
  projects: [
    {
      image: Images.TOYOTA_LOGO,
      projectName: "Project Toyota Car Maintenance Process Management",
      projectDesc: "",
      footerLink: [
        {
          name: "Visit Website",
          url: "http://saayahealth.com/",
        },
      ],
    },
    {
      image: Images.MODULE_1,
      projectName: "Game Space Invaders",
      projectDesc: "",
      footerLink: [
        {
          name: "Visit Website",
          url: "https://hacongbang174.github.io/CaseStudy_Game_Space_Invaders/",
        },
      ],
    },
    {
      image: Images.MODULE_2,
      projectName: "Phúc Long Coffee Management",
      projectDesc: "",
      footerLink: [
        {
          name: "Visit Github",
          url: "https://github.com/hacongbang174/CaseStudy-Module2",
        },
      ],
    },
    {
      image: Images.MODULE_3,
      projectName: "HCB Shop Management",
      projectDesc: "",
      footerLink: [
        {
          name: "Visit Github",
          url: "https://github.com/hacongbang174/CaseStudy-Module3",
        },
      ],
    },
    {
      image: Images.MODULE_4,
      projectName: "Figures Shop Management",
      projectDesc: "",
      footerLink: [
        {
          name: "Visit Github",
          url: "https://github.com/hacongbang174/Figures_Store_Case_M4",
        },
      ],
    },
  ],
  display: true,
};

const achievementSection = {
  title: emoji("Achievements And Certifications 🏆 "),
  subtitle:
    "Achievements, Certifications and Some Cool Stuff that I have done !",

  achievementsCards: [
    {
      title: "Certificate for Code.org Course",
      subtitle: "Completion Certificate for Code.org Course",
      image: Images.CODE_ORG,
      imageAlt: "Code.org Logo",
      footerLink: [
        {
          name: "Certification",
          url: "https://drive.google.com/drive/u/1/folders/1lluGofXVKn9nwbKghTxYO4suYegbKx07",
        },
      ],
    },
    {
      title: "Git Source Code Management Certificate",
      subtitle:
        "Git Source Code Management Certificate, Completion Certificate for How to Learn Course, Scrum Essence Course Completion Certificate, Kanban Course Completion Certificate",
      image: Images.CODE_GYM,
      imageAlt: "Codegym logo",
      footerLink: [
        {
          name: "Certification",
          url: "https://drive.google.com/drive/u/1/folders/1lluGofXVKn9nwbKghTxYO4suYegbKx07",
        },
      ],
    },
  ],
  display: true,
};

const contactInfo = {
  title: emoji("Contact Me ☎️"),
  subtitle: "",
  number: "+84 932 555 266",
  email_address: "hacongbang174@gmail.com",
};

const isHireable = false; // Set false if you are not looking for a job. Also isHireable will be display as Open for opportunities: Yes/No in the GitHub footer

export {
  illustration,
  greeting,
  socialMediaLinks,
  splashScreen,
  skillsSection,
  educationInfo,
  techStack,
  workExperiences,
  openSource,
  bigProjects,
  achievementSection,
  contactInfo,
  isHireable,
};
