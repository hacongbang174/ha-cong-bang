import "./App.module.scss";
import Main from "./containers/Main";

function App() {
  return (
    <div>
      <Main />
    </div>
  );
}

export default App;
