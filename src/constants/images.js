import codeGym from "../assets/images/codegym.webp";
import logoBKHCM from "../assets/images/HCMUT_official_logo.png";
import logoToyota from "../assets/images/toyota.jpg";
import codeOrg from "../assets/images/Code.org_logo.svg.png";
import module1 from "../assets/images/game-lai-may-bay-1.jpg";
import module2 from "../assets/images/phuc-long.jpg";
import module3 from "../assets/images/logohcb.png";
import module4 from "../assets/images/figure.png";

const Images = {
  CODE_ORG: codeOrg,
  CODE_GYM: codeGym,
  BKHCM_LOGO: logoBKHCM,
  TOYOTA_LOGO: logoToyota,
  MODULE_1: module1,
  MODULE_2: module2,
  MODULE_3: module3,
  MODULE_4: module4,
};

export default Images;
